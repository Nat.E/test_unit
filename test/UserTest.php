<?php

namespace App\Test;

use App\User;
use InvalidArgumentException;

class UserTest extends \PHPUnit\Framework\TestCase
{
    public function testTellName() {
        $user = new User(19,'Francis');
        self::assertEquals(
            "My name is Francis.",
            $user->tellName()
        );
    }

    public function testTellAge(){
        $user = new User(19,'Francis');
        self::assertEquals(
            "I am 19 years old.",
            $user->tellAge()
        );
    }

    public function testAddFavoriteMovie(){
        $user = new User(19,'Francis');
        self::assertEquals(
            true,
            $user->addFavoriteMovie('Avatar')
        );
        self::assertEquals(
            ['Avatar'],
            $user->favorite_movies
        );
    }

    public function testRemoveFavoriteMovie(){
        $user = new User(19,'Francis');
        $user->addFavoriteMovie('Avatar');
        self::assertEquals(
            true,
            $user->removeFavoriteMovie('Avatar')
        );
        self::assertEquals(
            [],
            $user->favorite_movies
        );
    }

    public function testRemoveFavoriteMovieExcepte(){
        $user = new User(19,'Francis');
        self::expectException(InvalidArgumentException::class);
        $user->removeFavoriteMovie('Avatar');
    }

}