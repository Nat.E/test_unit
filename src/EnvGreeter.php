<?php
namespace App;

/**
 * @method attach(Greeter|Greeter &\PHPUnit\Framework\MockObject\MockObject|\PHPUnit\Framework\MockObject\MockObject$envGreeter)
 */
class EnvGreeter extends Greeter {
	public function greetFromEnv(string $environmentVariableName):string {
		$name = getenv($environmentVariableName);
		return $this->greet($name);
	}
}
